﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.Json;

namespace Launchmanager
{
    class Program
    {
        private static string json_file = "config.json";
        private static Json ReadJson(string file)
        {
            string jsonData = File.ReadAllText(file);
            var ob = JsonSerializer.Deserialize<Json>(jsonData);
            return ob;

        }

        private static void ProcessStarter(string name, string args = "")
        {
            var reader = ReadJson(json_file);
            using (var p = new Process())
            {
                p.StartInfo.FileName = name;
                var split = args.Split(",");
                foreach (var s in split)
                {
                    p.StartInfo.ArgumentList.Add(s);
                }
                p.Start();
                p.WaitForExit();

                if (p.HasExited)
                {
                    var omni = reader.app.name;
                    Console.WriteLine("{0} exited checking for additional instances...", omni);

                    Process[] process = Process.GetProcessesByName(omni);
                    foreach (Process x in process)
                    {
                        x.Kill();
                        x.WaitForExit();
                        x.Dispose();
                    }
                    Console.Write("Additional instances of {0} stopped. Press any key to exit.", omni);
                    Console.ReadLine();
                    Environment.Exit(0);
                }
            }
        }

        static void Main()
        {
            var reader = ReadJson(json_file);
            var launcherexe = String.Format("{0}.exe", reader.app.name);
            var parentDirectory = Directory.GetParent(Environment.CurrentDirectory).ToString();
            var launcher = Path.Combine(parentDirectory, launcherexe);
            var omni = reader.app.name;
            Process[] process = Process.GetProcessesByName(omni);
            if (process.Length == 0)
            {
                Console.WriteLine("Starting: {0}", launcher);
                ProcessStarter(launcher);
            }
            else
            {
                foreach (var p in process)
                {
                    Console.WriteLine("Detected multiple instances of {0} stopping... Restart Launchmanager. Press any key to exit.", p.ProcessName);
                    p.Kill();
                }
                Console.ReadLine();
                Environment.Exit(0);
            }
        }
    }
}
