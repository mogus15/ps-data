# Launch manager

This script aims to resolve the multiple instances issue with https://github.com/justkeepquiet/tera-launcher using https://github.com/justkeepquiet/tera-api.

If multiple tera launchers are active the client patcher fails as files are being read and cannot be accessed. See images below. To resolve this you'll need to manually killed other instances running in the background.

## How to use

Place the Launchmanager folder inside client directory. Ensure this is placed within the same directory the `launcher.exe` is placed in. 

From there go into Launchmanager folder and start `Launchmanger.exe` this will launch the `launcher.exe` and kill additional instances if they are running the background. 

If multiple instances are not running then it will simply start `launcher.exe` and keep only one instance active.

## Download
https://gitlab.com/mogus15/datasheet/-/raw/main/scripts/Launchmanager/download/Launchmanager.zip?inline=false

## Images
![Patch Error](https://i.imgur.com/rwwfMFJ.png)
![Server Cab Error](https://i.imgur.com/0Mz1vHK.png)

