﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace hasher
{
    internal class Program
    {
        private static string sha512(string plaintext)
        {
            SHA512 sha512 = SHA512.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(plaintext);
            byte[] hash = sha512.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }

        static void Main(string[] args)
        {
            Console.Write("Password: ");
            var input = Console.ReadLine();
            if (Regex.IsMatch(input, "^[a-zA-Z0-9]*$") && input.Length >= 8)
            {
                Console.WriteLine("Hash: {0}", sha512(input));
                File.WriteAllText("hash.txt", sha512(input));
                Console.WriteLine("Press any key to exit.");
                Console.ReadLine();
            }
            else
            {
                Console.WriteLine("Please enter 8-14 character password. The following types are allowed [Numbers and Letters].");
                Console.WriteLine("Press any key to exit.");
                Console.ReadLine();
            }
        }
    }
}
