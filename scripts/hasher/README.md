# Hasher

Small console app to generate [SHA-512](https://komodoplatform.com/en/academy/sha-512/#:~:text=SHA%2D512%2C%20or%20Secure%20Hash,hashing%2C%20and%20digital%20record%20verification.) `hash` from a `string`. 

Program generates a text file `hash.txt` with the hashed password for easy sharing. This program aids in password resets as [tera-api](https://github.com/justkeepquiet/tera-api) or [tera-launcher](https://github.com/justkeepquiet/tera-launcher) do not handle password resets at the moment.
