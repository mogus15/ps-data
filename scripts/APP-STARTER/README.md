# APP Starter

## Information

Console application that will auto start the servers in order. Path to servers require [double back slash](https://docs.microsoft.com/en-us/cpp/c-language/escape-sequences?view=msvc-170). 

* Features
    * Set path to server
    * Set arguments for startup
    * Set time to wait before starting next service.

* Servers supported
    * Hub
    * Hub Gateway
    * Arbiter Gateway
    * Nexus Server
    * Match Server
    * Xigncode Proxy Server
    * Arbiter Server
    * Topography Server
    * World Server
    * Battlefield Server
    * Partymatching Server
    * Dungeon Other Server
    * Dungeon Server
    * Box Api
    * Steer Hub
    * Steer Session 
    * Steer Mind    
    * Steer Cast
    * Steer Gateway
    * ~~Box Web~~
    * ~~Steer Web~~
    * ~~FCGI Gateway~~
    * ~~FCGI WebAPI~~


## Toggle Wait and Start 

* To update time to wait before starting next service increase or decrease **wait** (seconds) in `config.json`. 
* Arguments are comma seperated.
* To toggle specific server to **NOT** start leave path blank. Example below. 

```json
"hub": {
    "path": "",
    "args": "12345,123,456",
    "wait": 0
}
```
