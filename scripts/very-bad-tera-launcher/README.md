# Modified Menma&apos;s TERA Launcher

## Very Bad Tera Launcher

A Glorified Patcher for TERA.


### Requirements
- [Teralauncher.dll](https://github.com/menmaa/Teralauncher) for game launching
- MTLUpdater.exe
- NodeJS
- Webserver

### Installation
- `npm install`
- `npm start`

### Server Setup
- `create_manifest.js` = Create manifest file for launcher self-update.
- `create_hash.js` = Create hash list of subdirectories for client.
- `download` = File created from `create_hash.js` will serve as list of files that the launcher will download/patch.
- `latest` = Version file used when new update is avaliable (i.e version_number+1).

#### Launcher Self-Update
1. Download a web-server.
2. Create a folder called `launcher` in web-server.
3. Copy `very-bad-tera-launcher.exe` to `launcher` folder as `Launcher.exe`.
4. Create a zip of `Launcher.exe` in same folder.
5. Run `node create_manifest.js` to create a manifest file for launcher self-update.

#### Client 

1. Download a web-server
2. Place `tera-client` content in a folder called `Client` in web-server.
3. Run `node create_hash.js path/to/Client` against `Client` folder to create a hash list of subdirectories.
4. Place `download` file in web-server.
5. Create file called `latest` with value `1000` and place in web-server.

### Client Setup

#### Launcher 
1. Update values in `config.json` to point to your web-server.
2. Ensure that `Teralauncher.dll` is in the same directory as launcher.
3. Create a file called `version.txt` with value `1.6` and place in same directory as launcher.
4. Start `very-bad-tera-launcher.exe`. This should start downloading the Client files that were recently hashed.

### Project Fork

- `@tsukasaroot`: https://github.com/tsukasaroot/menmas-tera-launcher/releases/tag/V1.6.0

![image](https://gitlab.com/mogus15/datasheet/-/raw/main/scripts/very-bad-tera-launcher/src/images/launcher.png)
