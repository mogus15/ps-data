"use strict";

const DefaultSettings = {
    debug: false,
    commands: {
        broker: {
            data: 28,
            type: 1,
        },
        bank: {
            data: [26, 1],
        },
        gbank: {
            data: [26, 3],
        },
        pbank: {
            data: [26, 9],
        },
        wardrobe: {
            data: [26, 12],
        },
        vgs: {
            data: [49, 609],
            huntingZoneId: 183,
            templateId: 2058,
            niceName: 'vanguard supply',
        },
        vgc: {
            data: [49, 6090],
            huntingZoneId: 183,
            templateId: 2059,
            niceName: 'vanguard crystal',
        },
        gl: {
            data: [49, 6112],
            huntingZoneId: 63,
            templateId: 6112,
            niceName: 'guardian legion',
        },
        ss: {
            data: [9, 250],
            huntingZoneId: 183,
            templateId: 2109,
            niceName: 'specialty store'
        },
        dm: {
            data: [9, 111],
            huntingZoneId: 183,
            templateId: 2005,
            niceName: 'development merchant'
        },
        merch: {
            data: [9, 70310],
            huntingZoneId: 183,
            templateId: 2019,
        },
        kaia: {
            data: [20, 6115],
            huntingZoneId: 411,
            templateId: 7002,
        },
        fovarth: {
            huntingZoneId: 411,
            templateId: 7002,
            type: 3,
        },
        pegasus: {
            huntingZoneId: 599,
            templateId: 903,
            type: 3
        },
        sm: {
            data: [9, 190],
            huntingZoneId: 183,
            templateId: 2006,
            niceName: 'smelting merchant'
        }
    },
    npcs: {
        63: {},
        183: {},
        411: {},
        599: {},
    },
};

module.exports = function MigrateSettings(from_ver, to_ver, settings) {
    if (from_ver === undefined) {
        // Migrate legacy config file
        return Object.assign(Object.assign({}, DefaultSettings), settings);
    } else if (from_ver === null) {
        // No config file exists, use default settings
        return DefaultSettings;
    } else {
        // Migrate from older version (using the new system) to latest one
        switch(from_ver) {
            default:
                return DefaultSettings;
        }
    }
};