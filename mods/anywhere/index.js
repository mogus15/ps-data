const debugHooks = [['S_NPC_MENU_SELECT', '*'], ['S_REQUEST_CONTRACT', 1], ['C_NPC_CONTACT', '*'], ['S_DIALOG', 2]];

class Target {
    constructor(mod, name, data, type = 0) {
        this.mod = mod;
        this.name = name;
        this.data = data;
        this.type = type;
    }

    contact = () => {
        switch (this.type) {
            case 0: {//C_REQUEST_CONTRACT
                const [type, value, huntingZoneId, templateId] = this.data;
                const gameId = BigInt(this.mod.settings.npcs[huntingZoneId]?.[templateId] ?? 0);
                const data = Buffer.alloc(4);
                data.writeUInt32LE(value);
                if (huntingZoneId != undefined && gameId == 0) {
                    this.mod.command.message('NPC ID not yet loaded for ' + this.name + '.');
                } else {
                    this.mod.send('C_REQUEST_CONTRACT', 69, { type, gameId, value, data });
                }
                break;
            }
            case 1:  //S_NPC_MENU_SELECT
                this.mod.send('S_NPC_MENU_SELECT', 1, { type: this.data });
                break;
            case 2:  //C_NPC_CONTACT
            case 3: {//C_NPC_CONTACT dynamic
                const [huntingZoneId, templateId] = this.data;
                const gameId = BigInt(this.mod.settings.npcs[huntingZoneId]?.[templateId] ?? 0);
                if (gameId == 0) {
                    this.mod.command.message('NPC ID not yet loaded for ' + this.name + '.');
                }
                this.mod.send('C_NPC_CONTACT', 2, { gameId });
                break;
            }
        }
    }
}

class Anywhere {
    constructor(mod) {
        this.mod = mod;
        this.loadDefinitions();
        this.loadHooks();
        this.loadCommands();
    }

    loadCommands() {
        const commands = Object.keys(this.mod.settings.commands);
        for (const commandName of commands) {
            const command = this.mod.settings.commands[commandName];
            const data = command.huntingZoneId ? [...(command.data instanceof Array ? command.data : []), command.huntingZoneId, command.templateId] : command.data;
            const target = new Target(this.mod, command.niceName ?? commandName, data, command.type);

            if (command.templateId != undefined && this.mod.settings.npcs[command.huntingZoneId][command.templateId] === undefined) {
                this.mod.settings.npcs[command.huntingZoneId][command.templateId] = 0;
                this.mod.saveSettings();
            }

            this.mod.command.add(commandName, target.contact);
        }
        this.mod.command.add('anywhere', {
            $default: () => {
                this.mod.command.message('Available commands: ' + commands.join(', '));
            },
            debug: () => {
                this.mod.settings.debug = !this.mod.settings.debug;
                this.mod.saveSettings();
                this.mod.command.message(`Debug mode ${this.mod.settings.debug ? 'en' : 'dis'}abled.`);
            }
        });
    }

    loadDefinitions() {
        this.mod.dispatch.addDefinition('C_REQUEST_CONTRACT', 69, [
            ['name', 'refString'],
            ['data', 'refBytes'],
            ['type', 'int32'],
            ['gameId', 'uint64'],
            ['button', 'uint32'],
            ['name', 'string'],
            ['data', 'bytes']
        ], true);
    }

    loadHooks() {
        this.mod.hook('S_SPAWN_NPC', this.mod.majorPatchVersion >= 101 ? 12 : 11, this.sSpawnNpc.bind(this));
        for (const [name, ver] of debugHooks) this.mod.hook(name, ver, this.debugHook(name).bind(this));
    }

    debugHook(name) {
        return e => {
            if (!this.mod.settings.debug) return;
            console.log(name, e);
        }
    }

    sSpawnNpc(e) {
        if (this.mod.settings.npcs[e.huntingZoneId] === undefined) return;
        if (this.mod.settings.npcs[e.huntingZoneId][e.templateId] === undefined) return;
        this.mod.settings.npcs[e.huntingZoneId][e.templateId] = e.gameId.toString();
        this.mod.saveSettings();
    }
}

module.exports = Anywhere;