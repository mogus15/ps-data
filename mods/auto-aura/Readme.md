# auto-aura
Automatically enables auras (crit, mana and thrall) when playing mystic.

## Commands

Toolbox(/8) | Command description
--- | ---
**autoaura** | Module on/off (default: on).
**autoaura pvp** | Enabling aura for PvP instead of PvE (default: off).
