# Lancer-Aimbot-Leash-Invincibility

Never fail a Leash, Master Leash and Giga. 
Become Invincible for as long as you want.


Your leash wont cast until your crossair poits at a target.


Pledge of Protection to cast Invincibility, tap it twice to stop. ( do not mount or your client will crash )

# Commands
```
/8 lancer 
/8 bg
```

# Config.json
```
enable: true,
debug: false,
bg: true,
bg_description: 'Enable/disable usage in bgs.',
time: 1200,
time_description: 'The amount of time you are invincible in ms.',
effect: 2060, 
effect_description: 'Effect id to apply while invincible.' 
(can be change for almost any effect, use effects module to find them)
```


