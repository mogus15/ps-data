'use strict'

const HOOK_LAST = { order: 100, filter: { fake: null } }

module.exports = function ItemCacheLight(mod) {

	let gameId = -1n,
		lock = false,
		itemlist = new Map(),
		itemList_temporary = null

	mod.hook('S_LOGIN', 14, event => {
		({ gameId } = event)
		itemlist.clear()
		itemList_temporary = null
	})
	//let x = true
	//const logIt = m => console.log(`[LCL] ${m}`)
	//const utils = require('util')
	mod.hook('S_ITEMLIST', 'raw', HOOK_LAST, (code, data) => {
		const event = {
			_lengthPacket: data.readUInt16LE(0),
			_opcodePacket: data.readUInt16LE(2),
			_countItems: data.readUInt16LE(4),
			_offsetItems: data.readUInt16LE(6),
			gameId: BigInt(data.readUInt32LE(8)) | BigInt(data.readUInt32LE(12)) << 32n,
			container: data.readInt32LE(16),
			pocket: data.readInt32LE(20),
			numPockets: data.readInt32LE(24),
			size: data.readInt32LE(28),
			money: BigInt(data.readInt32LE(32)) | BigInt(data.readInt32LE(36)) << 32n,
			lootPriority: data.readInt32LE(40),
			open: (!!data.readUInt8(44)),
			requested: (!!data.readUInt8(45)),
			first: (!!data.readUInt8(46)),
			more: (!!data.readUInt8(47)),
			lastInBatch: (!!data.readUInt8(48))
		}
		/*if(x) {
			logIt('-'.repeat('10'))
			logIt(`gId: ${gameId.toString()}n`)
			logIt('-'.repeat('10'))
			logIt(utils.inspect(event,false,null,false))
			logIt('-'.repeat('10'))
			logIt('-'.repeat('10'))
		}
		if(event.lastInBatch) x = false*/
		if (event.gameId !== gameId || event.requested) return
		//if(x) logIt(`Store called...`)
		if (lock) {
			itemlist.clear()
			if (event.lastInBatch) lock = false
			return
		}
		let returnValue = undefined

		if (!itemlist.has(event.container)) itemlist.set(event.container, new Map())
		const thisContainer = itemlist.get(event.container)

		if (!thisContainer.has('base')) thisContainer.set('base', {
			money: event.money,
			numPockets: event.numPockets,
			getting: true
		})
		const baseData = thisContainer.get('base')
		if (!baseData.getting && baseData.money !== event.money) lock = true
		if (!baseData.getting && baseData.numPockets !== event.numPockets) lock = true

		if (!lock) {
			if (!thisContainer.has(event.pocket)) thisContainer.set(event.pocket, { lengthPacket: event._lengthPacket, countItems: event._countItems, inven: [] })
			const thisPocket = thisContainer.get(event.pocket)
			const newBuffer = data.toString('hex')
			if (baseData.getting) {
				thisPocket.inven.push(newBuffer)
				if (event.lastInBatch) baseData.getting = false
				return
			}
			else Compare: {
				if (thisPocket.lengthPacket !== event._lengthPacket) { lock = true; /*logIt(`thisPocket.lengthPacket !== event._lengthPacket |=> ${thisPocket.lengthPacket} !== ${event._lengthPacket}`);*/ break Compare }
				if (thisPocket.countItems !== event._countItems) { lock = true; /*logIt(`thisPocket.countItems !== event._countItems |=> ${thisPocket.countItems } !== ${event._countItems}`);*/ break Compare }
				if (thisPocket.inven.indexOf(newBuffer) === -1) { lock = true; /*logIt(`thisPocket.inven.indexOf(newBuffer) === -1`);*/ break Compare }
				returnValue = false
				//logIt(`thisPocket.inven.indexOf(newBuffer) !== -1 !!!!!`)
			}
		}

		if (lock) {
			/*logIt(`lock called;L85`)
			logIt('-'.repeat('10'))
			logIt(utils.inspect(event,false,null,false))
			logIt('-'.repeat('10'))*/
			itemlist.clear()
			if (!event.first && itemList_temporary?.length)
				for (let data of itemList_temporary) mod.toClient(data)
			itemList_temporary = null
			if (event.lastInBatch) lock = false
			return
		}

		{ // Store
			//logIt(`Store called...`)
			if (event.first) itemList_temporary = []
			const newBuffer = Buffer.from(data)
			itemList_temporary.push(newBuffer)
			if (!event.more) itemList_temporary = null
		}

		if (returnValue === false) return false
	})
}