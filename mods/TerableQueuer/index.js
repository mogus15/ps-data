String.prototype.clr = function (hexColor) { return `<font color='#${hexColor}'>${this}</font>` };
module.exports = function TerableQueuer(mod) {
	const command = mod.command || mod.require.command;
	
	let enabled = true,
		instantExit = false,
		autoEnter = false,
		noticeChat = true,
		playerId = null,
		role = 1, // 0 = tank, 1 = dps, 2 = healer
		currentZone = null,
		lastZone = null,
		lastBG = null,
		instances = [],
		timeout = null,
		oldGID = 0n,
		sync = [],
		syncSpawned = [],
		ready = false,
		delay = 127,
		consoleMsg,
		t2 = null,
		n2 = null,
		leader = false,
		notifier = mod.manager.isLoaded('notifier') ? ( mod.require ? mod.require.notifier : require('tera-notifier')(mod) ) : false;

	command.add(['mz'], {
		$default(){
			command.message(`My zone = ${currentZone}`);
		}
	});
	
	command.add(['teraq', 'terableq'], {
		$default(){
			enabled = !enabled;
			command.message(`TerableQueuer is now ${enabled ? "enabled" : "disabled"}.`);
		},
		add(bgToAdd){
			if(bgToAdd && !isNaN(bgToAdd)){
				instances.push({ 
					"id": parseInt(bgToAdd),
					"type": 1,
					"matchingType": 0,
					"preferredLeader": leader
				});
			} else{
				command.message(`Failed to add ${bgToAdd}`);
			}
			command.message(`Currently added instances: `);
			for(let i = 0; i < instances.length; i++){
				command.message(`${instances[i].id}`);
			}
		},
		q(){
			command.message(`Queueing for: `);
			for(let i = 0; i < instances.length; i++){
				command.message(`${instances[i].id}`);
			}
			enabled = true;
			qBG();
		},
		clear(){
			instances = [];
			command.message(`BGs cleared`);
		},
		ae(){
			autoEnter = !autoEnter;
			command.message(`autoEnter = ${autoEnter}`);
		},
		ie(){
			instantExit = !instantExit;
			command.message(`instantExit = ${instantExit}`);
		},
		notice(){
			noticeChat = !noticeChat;
			command.message(`noticeChat = ${noticeChat}`);
		},
		leader(){
			leader = !leader;
			command.message(`leader = ${leader}`);
		},
		sync(playerName, add = false){
			if(playerName){
				playerName = playerName.toLowerCase();
				let argumentIndex = sync.indexOf(playerName);
				let spawnedIndex = syncSpawned.indexOf(playerName);
				if(add && argumentIndex == -1) sync.push(playerName); // if not in list, add to list
				else if(!add && argumentIndex > -1) sync.splice(argumentIndex, 1); // if in list, remove from list
				if(!add && spawnedIndex > -1) syncSpawned.splice(spawnedIndex, 1); // if in list, remove from list
			}
			command.message(`Sync Members: ${sync.toString()}`);
		},
		delay(theDelay){
			if(!theDelay || isNaN(theDelay)){
				command.message(`Invalid delay: ${theDelay}`);
				return;
			}
			delay = parseInt(theDelay);
			command.message(`Delay: ${delay}`);
		},
		ready(){
			lastBG = currentZone;
			ready = true;
			command.message(`BG zone: ${lastBG}`);
		},
		inbg(){
			lastBG = currentZone;
			ready = true;
			command.message(`BG zone: ${lastBG}`);
		},
		consoleMsg(){
			consoleMsg = !consoleMsg;
			command.message(`consoleMsg = ${consoleMsg ? "enabled" : "disabled"}.`);
		},
		help(){
			command.message(`teraq add 10`);
			command.message(`teraq q`);
			command.message(`5: Kumasylum`);
			command.message(`10: Corsair's Stronghold`);
			command.message(`11: Corsair's Stronghold (Premade)`);
			command.message(`26: Fraywind Canyon`);
			command.message(`27: Fraywind Canyon (Premade)`);
			command.message(`30: Shore Hold`);
			command.message(`37: Champion's Skyring (Team)`);
			command.message(`38: Champion's Skyring (Solo)`);
			command.message(`39: Champion's Skyring V (Team)`);
			command.message(`40: Champion's Skyring V (Solo)`);
			command.message(`46: Wintera Snowfield`);
			command.message(`47: Wintera Snowfield again?`);
			command.message(`70: Gridiron`);
		},
		print(){
			command.message(`enabled: ${enabled}`);
			command.message(`instantExit: ${instantExit}`);
			command.message(`noticeChat: ${noticeChat}`);
			command.message(`currentZone: ${currentZone}`);
			command.message(`lastZone: ${lastZone}`);
			command.message(`leader: ${leader}`);
			command.message(`lastBG: ${lastBG}`);
			command.message(`instances: `);
			for(let i = 0; i < instances.length; i++){
				command.message(`${instances[i].id}`);
			}
			command.message(`Sync Members: ${sync.toString()}`);
			command.message(`Sync Members Spawned: ${syncSpawned.toString()}`);
			command.message(`Delay: ${delay}`);
		}
	});
	
	function queueBG(){
		mod.clearTimeout(timeout);
		timeout = mod.setTimeout(qBG, delay);
	}
	
	function qBG(){
		ready = false;
		mod.send('C_ADD_INTER_PARTY_MATCH_POOL', 3, {
			unk1: 0,
			unk2: 0,
			instances,
			players: [{
				id: playerId,
				role
			}]
		});
	}
	mod.game.on('enter_game', () => { 
		if(!mod.game.me.is(oldGID)){
			let job = (mod.game.me.templateId - 10101) % 100;
			if(job == 1 || job == 10) role = 0;
			else if(job == 6 || job == 6) role = 2;
			//Class#: warrior = 0, lancer = 1, slayer = 2, berserker = 3, sorcerer = 4, archer = 5,
			//priest = 6, mystic = 7, reaper = 8, gunner = 9, brawler = 10, ninja = 11, valkyrie = 12
			playerId = mod.game.me.playerId;
			oldGID = mod.game.me.gameId;
			currentZone = null;
			lastZone = null;
			lastBG = null;
		}
	});
	mod.hook('S_RETURN_TO_LOBBY', 'raw', () => {
		mod.clearTimeout(timeout);
		timeout = null;
	});
	
	mod.hook('C_ADD_INTER_PARTY_MATCH_POOL', 3, e => {
		instances = [];
		let map1 = e.instances.map(x => x.id);
		for(let i = 0; i < map1.length; i++){
			instances.push({ 
				"id": map1[i],
				"type": 1,
				"matchingType": 0,
				"preferredLeader": leader
			});
		}
		command.message(`Queueing for: `);
		for(let i = 0; i < instances.length; i++){
			command.message(`${instances[i].id}`);
		}
	});
	
	mod.hook('S_BATTLE_FIELD_ENTRANCE_INFO', 1, e => { // ims popped
		ready = false;
		lastBG = e.zone;
		//if(notifier) notifier.messageafk(`BG Popped`);
		/*if(autoEnter){
			mod.setTimeout(() => {
    			mod.send('C_ENTER_BATTLE_FIELD', 1, {}); // enter bg
    		}, 1342); // 1000ms = 1s
			
		}*/
	});
	
	/*mod.hook('S_BATTLE_FIELD_RESULT', 1, e => { // bg over
		syncSpawned = [];
		if(instantExit) mod.setTimeout(() => { mod.send('C_BATTLE_FIELD_REWARD', 1, {}); }, 739); // 1000ms = 1s // leave bg
	});*/
	
	mod.hook('S_LOAD_TOPO', 3, e => {
		lastZone = currentZone;
		currentZone = e.zone;
	});
	
	mod.hook('S_SPAWN_USER', 16, e => { // sync q
		if(sync.includes(e.name.toLowerCase()) && !syncSpawned.includes(e.name.toLowerCase())){
			syncSpawned.push(e.name.toLowerCase());
			if(lastZone != null && lastBG != null && lastZone == lastBG && lastZone != currentZone) ready = true;
			if(sync.length === syncSpawned.length && enabled && ready) queueBG(); // if ready and last zone was bg, re Q
		}
	});
	
	mod.hook('C_VISIT_NEW_SECTION', 1, e => { // C_LOAD_TOPO_FIN is too fast
		if(lastZone != null && lastBG != null && lastZone == lastBG && lastZone != currentZone) ready = true;
		if(sync.length === syncSpawned.length && enabled && ready) queueBG(); // if last zone was bg, re Q
	});
	
	function timeStamp(msg){
		let timeNow = new Date();
		let timeText = timeNow.getHours().toLocaleString(undefined, {minimumIntegerDigits: 2}) + ':' +
			timeNow.getMinutes().toLocaleString(undefined, {minimumIntegerDigits: 2}) + ':' + 
			timeNow.getSeconds().toLocaleString(undefined, {minimumIntegerDigits: 2});
		return ("[" + timeText + "] ");
	}

	function nofity(msg, zone){
		if(currentZone != zone){
			mod.clearTimeout(t2);
			mod.clearTimeout(n2);
			return;
		}
		mod.send('S_CHAT', 3, { channel: 21, authorName: '', message: msg });
		mod.send('S_CHAT', 3, { channel: 21, authorName: '', message: msg });
		mod.send('S_DUNGEON_EVENT_MESSAGE', 2, { type: 43, chat: 0, channel: 0, message: msg });
	}

	mod.hook('S_BATTLE_FIELD_STRONGHOLD_EVENT', 1, e => {
		//if(msg && (msg.id === 'SMT_LOST_DUNGEON_OWNERSHIP')){ // bg end?
			syncSpawned = [];
			//if(instantExit) mod.setTimeout(() => { mod.send('C_BATTLE_FIELD_REWARD', 1, {}); }, 339); // 1000ms = 1s // leave bg
			if(instantExit) mod.setTimeout(() => { mod.send('C_LEAVE_PARTY', 1, {}); }, 339); // 1000ms = 1s // leave bg
			//if(instantExit) mod.setTimeout(() => { mod.message("Exiting!!"); }, 339); // 1000ms = 1s // leave bg
			if(notifier) notifier.messageafk(`BG Done`);
		//}
	});

	mod.hook('S_SYSTEM_MESSAGE', 1, e => {
		const msg = mod.parseSystemMessage(e.message);
		if(consoleMsg){
			console.log(timeStamp() + "Logged Message");
			console.log(msg);
		}
		//if(msg && (msg.id === 'SMT_BF_KUMAS_GET_COMPENSATION')){ // bg end?
		/*if(msg && (msg.id === 'SMT_LOST_DUNGEON_OWNERSHIP')){ // bg end?
			syncSpawned = [];
			//if(instantExit) mod.setTimeout(() => { mod.send('C_BATTLE_FIELD_REWARD', 1, {}); }, 339); // 1000ms = 1s // leave bg
			if(instantExit) mod.setTimeout(() => { mod.message("Exiting!!"); }, 339); // 1000ms = 1s // leave bg
			if(notifier) notifier.messageafk(`BG Done`);
		}*/

		if(notifier && parseInt(msg.tokens.sec) == 5) notifier.message(`BG Starting`);
    	if(enabled && msg && (msg.id === 'SMT_CANT_ENTER_BATTLEFIELD_COOLTIME_PARTY' || msg.id === 'SMT_CANT_ENTER_BATTLEFIELD_COOLTIME')){ // on CD
			let time =  parseInt(msg.tokens.Min) * 60 + parseInt(msg.tokens.Sec);
			command.message(`On cooldown for ${time} seconds. Requeing in ${time+1} seconds.`);
			mod.clearTimeout(timeout);
			timeout = mod.setTimeout(qBG, (time+1)*1000); // 1000ms = 1s
		} else if(noticeChat && msg && (msg.id === 'SMT_BF_CHATMSG_PRE_START')){
			if(parseInt(msg.tokens.sec) == 5 && currentZone == 1200){ // SH
				mod.setTimeout(() => { 
					nofity(`Mini Soon...`.clr("F7347A"), 1200);
				}, 130000);
				mod.setTimeout(() => { 
					nofity(`Turtle Soon...`.clr("F7347A"), 1200);
				}, 310000);
			} else if(noticeChat && parseInt(msg.tokens.sec) == 5 && currentZone == 112){ // FWC
				mod.setTimeout(() => { 
					nofity(`3:40 Tera Soon...`.clr("F7347A"), 112);
				}, 180000); // 3:00
				mod.setTimeout(() => {
					nofity(`5:40 Naga Soon...`.clr("F7347A"), 112);
				}, 300000); // 5:00

				t2 = mod.setTimeout(() => { 
					nofity(`8:10 Tera Soon...`.clr("F7347A"), 112);
				}, 420000); // 7:00
				n2 = mod.setTimeout(() => { 
					nofity(`11:10 Naga Soon...`.clr("F7347A"), 112);
				}, 600000); // 10:00

				mod.setTimeout(() => { 
					nofity(`3:05 Red Soon...`.clr("F7347A"), 112);
				}, 130000); // 2:10
				mod.setTimeout(() => { 
					nofity(`6:15 Red Soon...`.clr("F7347A"), 112);
				}, 310000); // 5:10
			} 
		}
	});
}