# TerableQueuer

NOT updated for Menma's Tera

Make sure to drop C_ADD_INTER_PARTY_MATCH_POOL.2 into Proxy\node_modules\tera-data\protocol

Made by TerableCoder

## Usage
### `teraq` 
- Toggles instant re Queueing on or off, default off
### `teraq add(bgNumber)` 
- Adds a BG to the instant re Queue list
### `teraq delist all` or `teraq dl all` 
- Delist all brokered items
### `teraq q` 
- Queues for the BGs added to the list
### `teraq clear`
- Clears BG queue list
### `teraq ae`
- Toggles auto enter BG on or off, default on 
### `teraq ie`
- Toggles instant exit BG on or off, default on 
### `teraq help`
- Prints the commands to leave the BG and the BG numbers
### `teraq print`
- Prints variable information
