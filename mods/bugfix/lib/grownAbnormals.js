// Fixes a bug where patch 115 port causes grown Abnormals and missing abnormals on bosses.
// Written by: Kygas
// Credit : Snug

module.exports = function GrownAbnormalsFix(mod) {
	
	const Ignored_Abnorms = [
		10155020
	]
	
	let encounterId = []
	mod.hook("S_BOSS_GAGE_INFO", 3, e=>{
		if(!encounterId.includes(e.id)) encounterId.push(e.id)
	})
	
	mod.hook("S_LOAD_TOPO", 'raw', ()=>{
		encounterId = [];
	})
	
	mod.hook('S_ABNORMALITY_BEGIN', 4, { order: Infinity, filter: { fake: null } }, (e) => {
		if ( encounterId.includes(e.target) ) {
			process.nextTick(() => mod.send('S_ABNORMALITY_REFRESH', '*', e))
		}
	})

	mod.hook('S_ABNORMALITY_REFRESH', 2, { order: Infinity, filter: { fake: null } }, (e) => {
		if ( !encounterId.includes(e.target) && !Ignored_Abnorms.includes(e.id) ) {
			mod.send('S_ABNORMALITY_END', 1, e)
			mod.send('S_ABNORMALITY_BEGIN', 4, e)
			return false
		}
	})	
}