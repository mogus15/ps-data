module.exports = function Ninja(mod) {

  var command = mod.command;
  var { player } = mod.require.library;
  var skills = {};
  var enabled = true;
  var timeout = null;
  var config = require('./config.json');

  if (config.FIREAVALANCHE_CANCEL_DELAY && typeof config.FIREAVALANCHE_CANCEL_DELAY === 'number') {
    skills['8-51'] = {
      "delay": config.FIREAVALANCHE_CANCEL_DELAY,
    };
  }
  if (config.SMOKEBOMB_CANCEL_DELAY && typeof config.SMOKEBOMB_CANCEL_DELAY === 'number') {
    skills['9-31'] = {
      "delay": config.SMOKEBOMB_CANCEL_DELAY,
    };
  }
  if (config.CIRCLESTEEL_CANCEL_DELAY && typeof config.CIRCLESTEEL_CANCEL_DELAY === 'number') {
    skills['13'] = {
      "delay": config.CIRCLESTEEL_CANCEL_DELAY,
    };
  }
 if (config.CIRCLESTEELCHAINED_CANCEL_DELAY && typeof config.CIRCLESTEELCHAINED_CANCEL_DELAY === 'number') {
    skills['13-30'] = {
      "delay": config.CIRCLESTEELCHAINED_CANCEL_DELAY,
    };
  }
  if (config.SKYFALL_CHAINED_CANCEL_DELAY && typeof config.SKYFALL_CHAINED_CANCEL_DELAY === 'number') {
    skills['12-30'] = {
      "delay": config.SKYFALL_CHAINED_CANCEL_DELAY,
    };
  }
  if (config.SKYFALL_NATURAL_CANCEL_DELAY && typeof config.SKYFALL_NATURAL_CANCEL_DELAY === 'number') {
    skills['12-1'] = {
      "delay": config.SKYFALL_NATURAL_CANCEL_DELAY,
    };
  }
  if (config.BOOMERANG_CANCEL_DELAY && typeof config.BOOMERANG_CANCEL_DELAY === 'number') {
    skills[23] = {
      "delay": config.BOOMERANG_CANCEL_DELAY
    };
  }
  if (config.DOUBLE_CUT_CANCEL_DELAY && typeof config.DOUBLE_CUT_CANCEL_DELAY === 'number') {
    skills[14] = {
      "delay": config.DOUBLE_CUT_CANCEL_DELAY
    };
  }
  if (config.ATTUNEMENT_CANCEL_DELAY && typeof config.ATTUNEMENT_CANCEL_DELAY === 'number') {
    skills[17] = {
      "delay": config.ATTUNEMENT_CANCEL_DELAY
    };
  }
  if (config.LEAVES_CANCEL_DELAY && typeof config.LEAVES_CANCEL_DELAY === 'number') {
    skills[3] = {
      "delay": config.LEAVES_CANCEL_DELAY
    };
  }
  if (config.QUICK_ATTACK_CHAINED_CANCEL_DELAY && typeof config.QUICK_ATTACK_CHAINED_CANCEL_DELAY === 'number') {
    skills['22-10'] = {
      "delay": config.QUICK_ATTACK_CHAINED_CANCEL_DELAY
    };
  }
  if (config.QUICK_ATTACK_NATURAL_CANCEL_DELAY && typeof config.QUICK_ATTACK_NATURAL_CANCEL_DELAY === 'number') {
    skills['22-30'] = {
      "delay": config.QUICK_ATTACK_NATURAL_CANCEL_DELAY
    };
  }
  if (config.IMPACT_BOMB_CANCEL_DELAY && typeof config.IMPACT_BOMB_CANCEL_DELAY === 'number') {
    skills[5] = {	
      "delay": config.IMPACT_BOMB_CANCEL_DELAY
    };
  }
  if (config.CHAKRA_THRUST_CANCEL_DELAY && typeof config.CHAKRA_THRUST_CANCEL_DELAY === 'number') {
    skills[19] = {
      "delay": config.CHAKRA_THRUST_CANCEL_DELAY
    };
  }


  command.add('ninja', {
    $default() {
      enabled = !enabled;
      command.message('Ninja ' + (enabled ? 'activated' : 'deactivated') + " uguuu");
    }
  });

  mod.hook('S_ACTION_STAGE', 9, {
    "order": -1000000,
    "filter": {
      "fake": true
    }
  }, function (event) {
    if (!enabled || event.gameId !== mod.game.me.gameId || mod.game.me.class !== 'assassin') {
      return;
    }
    var skllId = Math.floor(event.skill.id / 10000);
    var skillSubId = event.skill.id % 100;
    if (skllId in skills || skllId + "-" + skillSubId in skills) {
      var skill = skllId in skills ? skills[skllId] : skills[skllId + "-" + skillSubId];
      timeout = mod.setTimeout(function () {
        mod.toClient('S_ACTION_END', 5, {
          "gameId": event.gameId,
          "loc": {
            "x": event.loc.x,
            "y": event.loc.y,
            "z": event.loc.z
          },
          "w": event.w,
          "templateId": event.templateId,
          "skill": event.skill.id,
          "type": 12394123,
          "id": event.id
        });
      }, skill.fixedDelay ? skill.delay : skill.delay / player.aspd);
    }
  });

  mod.hook('S_ACTION_END', 5, {
    "order": -1000000,
    "filter": {
      "fake": true
    }
  }, function (event) {
    if (!enabled || event.gameId !== mod.game.me.gameId || mod.game.me.class !== 'assassin') {
      return;
    }
    var skillId = Math.floor(event.skill.id / 1E4);
    var skillSubId = event.skill.id % 100;
    if (timeout && (skillId in skills || skillId + "-" + skillSubId in skills)) {
      timeout = null;
      if (event.type == 12394123) {
        event.type = 4;
        return true;
      } else {
        return false;
      }
    }
  });

  mod.hook('C_CANCEL_SKILL', 3, function () {
    if (!enabled || mod.game.me.class !== 'assassin') {
      return;
    }
    if (timeout) {
      mod.clearTimeout(timeout);
      timeout = null;
    }
  });

  mod.hook('S_EACH_SKILL_RESULT', 14, {
    "order": -10000000
  }, function (event) {
    if (!enabled || !timeout || event.target !== mod.game.me.gameId || !event.reaction.enable) {
      return;
    }
    mod.clearTimeout(timeout);
    timeout = null;
  });
}
