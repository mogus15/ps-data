module.exports = [
/*
	// Enrage Added
	{
		type: 'added',
		target: 'MyBoss',
		abnormalities: 8888888,
		message: 'Enrage {duration}'
	},	
*/
	// Contagion Added
	{
		type: 'added',
		target: 'MyBoss',
		abnormalities: [701700, 701701],
		message: '{icon} {duration}'
	},
/*
	// Contagion Expiring, notify 6 seconds remaining
	{
		type: 'expiring',
		target: 'MyBoss',
		abnormalities: [701700, 701701],
		message: '{icon} {duration}',
		time_remaining: 6
	},
	// Contagion Removed
	{
		type: 'removed',
		target: 'MyBoss',
		abnormalities: [701700, 701701],
		message: '{icon} Ended'
	},
*/

	// Hurricane Added
	{
		type: 'added',
		target: 'MyBoss',
		abnormalities: 60010,
		message: 'Hurricane {duration}'
	},
/*
	// Hurricane Expiring, notify at 6 seconds remaining
	{
		type: 'expiring',
		target: 'MyBoss',
		abnormalities: 60010,
		message: 'Hurricane {duration}',
		time_remaining: 6
	},
	// Hurricane Removed
	{
		type: 'removed',
		target: 'MyBoss',
		abnormalities: 60010,
		message: 'Hurricane Ended'
	},
*/

	// Adrenaline Rush  Added
	{
		type: 'added',
		target: 'Self',
		abnormalities: [200701, 200700],
		message: '{icon} {duration}'
	},
/*
	// Adrenaline Rush  Expiring, notify at 6 seconds remaining
	{
		type: 'expiring',
		target: 'Self',
		abnormalities: [200701, 200700],
		message: '{icon} {duration}',
		time_remaining: 6
	},
	// Adrenaline Rush Removed
	{
		type: 'removed',
		target: 'Self',
		abnormalities: [200701, 200700],
		message: '{icon} Ended'
	},
*/

	// Missing Battle Solution / Nostrum
	{
		type: 'MissingDuringCombat',
		target: 'Self',
		abnormalities: [4030, 6090, 6091, 6092, 4031, 4020, 4021, 4022, 4042, 4040],
		message: 'Missing {icon}',
		rewarn_timeout: 15
	},
/*
	// Vergos Aggro Debuff
   {
		type: 'AddedOrRefreshed',
		target: 'PartyIncludingSelf',
		abnormalities: 950023,
		message: '{name} has {icon} {stacks} stack(s)',
		required_stacks: 1
	},

	// Vergos Aggro Debuff Expire
   {
		type: 'Removed',
		target: 'PartyIncludingSelf',
		abnormalities: 950023,
		message: '{name}\'s {icon} stacks expired'
	},
*/
     // Priest edict  Added
	{
		type: 'added',
		target: 'Self',
		abnormalities: [805803],
		message: '{icon} {duration}'
	},

	// Mystic Wrath  Added
	{
		type: 'added',
		target: 'Self',
		abnormalities: [702004],
		message: '{icon} {duration}'
	},
	
/*	// Mystic Vengeance  Added
	{
		type: 'added',
		target: 'Self',
		abnormalities: [702003],
		message: '{icon}Vengeance {duration}'
	},
		
	// Priest Divine Charge  Added
	{
		type: 'added',
		target: 'Self',
		abnormalities: [ 805711, 805712, 805713],
		message: '{icon}Divine Charge {duration}'
	},
	
	// Holy Burst  Added
	{
		type: 'added',
		target: 'Self',
		abnormalities: [806021, 806021],
		message: '{icon} {duration}'
	},
*/
	// Kaia  Added
	{
		type: 'added',
		target: 'Self',
		abnormalities: [800300, 800302, 800303, 800304],
		message: '{icon} on'
	},
	
	// Mystic Shield  Added
	{
		type: 'added',
		target: 'Self',
		abnormalities: [702001],
		message: '{icon} on'
	},
	
	// Bahaar Laser
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 90442502,
		message: '{name} has {icon}'
	},
	
	// Sea Stun
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 30209101,
		message: '{name} has {icon}'
	},
	
	// Sea Fear
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 30209102,
		message: '{name} has {icon}'
	},
	
	// Lumikan Immunity
    {
		type: 'Added',
		target: 'Self',
		abnormalities: [31040003, 32040003],
		message: '{icon} {duration}'
	},
	
	// Lumikan HM target
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 32040007,
		message: '{icon} {name}'
	},
	
	// Gardan HM target
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 32060024,
		message: '{icon} {name}'
	},
	
	// Antaroth stacks
    {
		type: 'Added',
		target: 'MyBoss',
		abnormalities: 31083063,
		message: '{icon} 1!'
	},	
	
	// Antaroth stacks
    {
		type: 'AddedorRefreshed',
		target: 'MyBoss',
		abnormalities: 31083063,
		message: '{icon} {stacks}!'
	},
	
	// Antaroth stacks
    {
		type: 'Removed',
		target: 'MyBoss',
		abnormalities: 31083063,
		message: '{icon} 4!!!!!!'
	},	
	
	// Valk buffs
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 10155130,
		message: '{icon} {name}'
	},
	
	// Valk buffs
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 10155512,
		message: '{icon} {name}'
	},
	
	//  Ninja buffs
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 10154480,
		message: '{icon} {name}'
	},
	
	// Reaper shadow reaping
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 10151010,
		message: '{icon} {name}'
	},
	
	// Reaper assassinate
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 10151192,
		message: '{icon} {name}'
	},
	
	// Sorc buffs
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 503061,
		message: '{icon} {name}'
	},
	
	// Warrior buffs
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 100801,
		message: '{icon} {name}'
	},
	
	// Gunner buffs
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 10152340,
		message: '{icon} {name}'
	},
	
	// Archer buffs
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 602221,
		message: '{icon} {name}'
	},
	
	// Slayer buffs
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 300808,
		message: '{icon} {name}'
	},
	
	// Zerk buffs
    {
		type: 'Added',
		target: 'PartyIncludingSelf',
		abnormalities: 401705,
		message: '{icon} {name}'
	},
	
	/*// Manaya 1
    {
		type: 'Added',
		target: 'Self',
		abnormalities: 30471008,
		message: 'Wipe 4 Annihilate 2!'
	},

	// Manaya 2
    {
		type: 'Added',
		target: 'Self',
		abnormalities: 30471009,
		message: 'Wipe 1 Annihilate 3!'
	},	

	// Manaya 3
    {
		type: 'Added',
		target: 'Self',
		abnormalities: 30471010,
		message: 'Wipe 2 Annihilate 4!'
	},

	// Manaya 4
    {
		type: 'Added',
		target: 'Self',
		abnormalities: 30471011,
		message: 'Wipe 3 Annihilate 1!'
	},*/
]
