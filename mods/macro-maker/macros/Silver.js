module.exports = {
	enabled: false,
	toggleRepeaterKey: "\\",
	skills: {

	  "18": { //shield Barrage
             enabled: true,
             key: "e",
             onPress: [
				{ action: "keyTap", key: "space", duration: 1, delay: 200, inCombat: true, enableIfSkillCooldown: ["25","3"]},
				{ action: "keyTap", key: "4", duration: 1, delay: 200, inCombat: true, enableIfSkillCooldown: "25"},
				{ action: "keyTap", key: "tab", duration: 1, delay: 200, inCombat: true,}
           ],
             repeater: false,
         },

	},
	hotkeys: {
	}
}
///enableIfAbnormality: "10151016",
