module.exports = {
    enabled: true,
    toggleRepeaterKey: "ScrollLock",
    skills: {
		// Combo Attack
        "1": {
			enabled: false,
            key: "lbutton",
            repeater: false,
		},
		
		// Shadow Jump
		"2": {
			enabled: false,
            key: "rbutton",
            repeater: false,
		},
		
		// Leaves on the Wind
		"3": {
			enabled: true,
            key: "F3",
            repeater: true,
		},
		
		// Jagged Path
		"4": {
			enabled: false,
            key: "tab",
            repeater: false,
		},
		
		// Impact Bomb
		"5": {
			enabled: true,
            key: "F1", 
            repeater: true,
		},
		
		// One Thousand Cuts
		"6": {
			enabled: false,
            key: "4",
            repeater: false,
		},
		
		// Decoy jitsu
		"7": {
			enabled: true,
            key: "c",
            repeater: true,
		},
		
		// Fire Avalanche
		"8": {
			enabled: true,
            key: "q",
            repeater: true,
		},
		
		// Smoke Bomb
		"9": {
			enabled: true,
            key: "alt+lbutton",
            repeater: true,
		},
		
		// Retaliate
		"10": false,

		// Focus
		"11": {
			enabled: false,
            key: "alt+rbutton",
            repeater: false,
		},
		
		// Skyfall
		"12": {
			enabled: true,
            key: "2",
            repeater: true,
		},
		
		// Circle of Steel
		"13": {
			enabled: true,
            key: "3",
            repeater: true,
		},
		
		// Double Cut
		"14": {
			enabled: true,
            key: "1",
            repeater: true,
		},
		
		// Burning Heart
		"15": {
			enabled: true,
            key: "6",
            repeater: true,
		},
		
		// Death Blossom
		"16": {
			enabled: false,
            key: "alt+e",
            repeater: false,
		},
		
		// Attunement
		"17": {
			enabled: true,
            key: "F4",
            repeater: true,
		},
		
		// Bladestorm
		"18": {
			enabled: false,
            key: "5",
            repeater: false,
		},
		
		// Chakra Thrust
        "19": {
            enabled: true,
            key: "r",
            repeater: false,
            onCast: [
                    { action: "keyRepeat", key: "e", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10154480", stopOnNextCast: true},
                    { action: "keyRepeat", key: "e", duration: 1000, interval: 1, delay: 1, inCombat: true, disableIfSkillCooldown:"22", stopOnNextCast: true},
                    ],
        },
		
		// Clone Jutsu
		"20": false,
		
		
		// Boomerang Shuriken
		"21": {
			enabled: true,
            key: "F2",
            repeater: true,
		},
		
		// Quick Attack
        "22": {
            enabled: true,
            key: "e",
            repeater: true,
            onCast: [
                    { action: "keyRepeat", key: "r", duration: 1000, interval: 1, delay: 1350, inCombat: true, enableIfAbnormality: "10154480", stopOnNextCast: true},
                    ],
        },
		
		// Inner Harmony
		"23": {
			enabled: false,
            key: "t",
            repeater: false,
		},
		
		// Apex Urgency
		"910": {
			enabled: false,
            key: "x",
            repeater: false,
		},
		
    },
    hotkeys: {
    }
}
