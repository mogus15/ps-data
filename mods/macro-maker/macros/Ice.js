
module.exports = {
	enabled: true,
	toggleRepeaterKey: "\\",
	skills: {
        "24": {//1 inch punch
			enabled: true,
			key: "e",
            onPress: [
            { action: "keyTap", key: "space", duration: 1, delay: 0, inCombat:true,  disableIfSkillCooldown: ["7"], disableIfAbnormality: "31110"},//rhk
            { action: "keyTap", key: "f", duration: 1, delay: 0, inCombat:true,  enableIfSkillCooldown: ["24","6"], enableIfAbnormality: "10153000"},// counter punch
            { action: "keyTap", key: "t", duration: 1, delay: 0, inCombat:true, enableIfSkillCooldown: ["24","22", "6","9","8"]},//auto attack
			{ action: "keyTap", key: "RButton", duration: 1, delay: 0, inCombat: true, enableIfSkillCooldown: ["24","6","9","8"] },//flying kick
            { action: "keyTap", key: "r", duration: 1, delay: 0, inCombat:true, enableIfSkillCooldown: ["24", "6","8"] },//piledriver
            { action: "keyTap", key: "q", duration: 1, delay: 0, inCombat:true, enableIfSkillCooldown: ["24", "6"] },//jackhammer
            { action: "keyTap", key: "LButton", duration: 1, delay: 0,  inCombat:true, enableIfSkillCooldown: "24" }//haymaker
			],
			repeater: false,
		},

	},
	hotkeys: {
	}
}
