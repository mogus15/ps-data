// module.exports = {
// 	enabled: true,
// 	toggleRepeaterKey: "\\",
// 	skills: {

// 		"3": {
// 			enabled: true,
// 			key: "3",
// 			repeater: false,
// 			onCast: [
//             { action: "keyRepeat", key: "Tab", duration: 500, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
// 			{ action: "keyRepeat", key: "Tab", duration: 500, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151010", stopOnNextCast: true },
//             ],
// 		},

// 		"4": {
// 			enabled: false,
// 			key: "Tab",
// 			repeater: false,
// 			onCast: [
//             { action: "keyRepeat", key: "space", duration: 1000, interval: 1, delay: 200, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
//             ],
// 		},

//         "5": {
// 			enabled: false,
// 			key: "t",
// 			repeater: false,
// 			onCast: [
// 				{ action: "keyRepeat", key: "Tab", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
// 				{ action: "keyRepeat", key: "Tab", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151010", stopOnNextCast: true },
// 				//{ action: "keyRepeat", key: "Tab", duration: 700, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true }
//             ],
// 		},

// 		"6": {
// 			enabled: true,
// 			key: "f",
// 			repeater: false,
// 			onCast: [
// 				{ action: "keyRepeat", key: "3", duration: 400, interval: 1, delay: 1, inCombat: true, stopOnNextCast: true },
// 			{ action: "keyRepeat", key: "3", duration: 400, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
// 			{ action: "keyRepeat", key: "3", duration: 400, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151010", stopOnNextCast: true },
//             //{ action: "keyRepeat", key: "t", duration: 400, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
//             { action: "keyRepeat", key: "3", duration: 400, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151192", stopOnNextCast: true },
//             ],
// 		},


// 		"12": {
//         	enabled: true,
//         	key: "4",
//         	repeater: false,
//         	onCast: [
// 				//{ action: "keyRepeat", key: "Tab", duration: 500, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
//             { action: "keyRepeat", key: "f", duration: 500, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
// 			{ action: "keyRepeat", key: "f", duration: 500, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151010", stopOnNextCast: true },
//             ],
//         },

// 		"19": {
// 			enabled: false,
// 			key: "LButton",
// 			repeater: false,
// 			onCast: [
//             //{ action: "keyRepeat", key: "t", duration: 800, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", enableIfSkillCooldown: "4", stopOnNextCast: true },
// 		  	{ action: "keyRepeat", key: "t", duration: 800, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016",enableIfSkillCooldown: ["12","4"], stopOnNextCast: true },
// 		  	{ action: "keyRepeat", key: "4", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016",enableIfSkillCooldown: "4", stopOnNextCast: true },
// 			  //{ action: "keyRepeat", key: "4", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151010",enableIfSkillCooldown: "12", stopOnNextCast: true },
// 			//{ action: "keyRepeat", key: "Tab", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151192",enableIfSkillCooldown: "12", stopOnNextCast: true },
//             { action: "keyRepeat", key: "Tab", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
// 			//{ action: "keyRepeat", key: "Tab", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151010", stopOnNextCast: true },
// 			//{ action: "keyRepeat", key: "4", duration: 800, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151192", stopOnNextCast: true }
//             ],
// 		},

//         "16": {
//     		enabled: true,
//     	    key: "g",
//     		repeater: false,
//     		onCast: [
//     		//{ action: "keyRepeat", key: "LButton", duration: 1000, interval: 1, delay: 0, inCombat: true, stopOnNextCast: true},
// 			{ action: "keyRepeat", key: "LButton", duration: 400, interval: 1, delay: 0, inCombat: true,enableIfAbnormality: "10151016", stopOnNextCast: true},
// 			{ action: "keyRepeat", key: "LButton", duration: 400, interval: 1, delay: 0, inCombat: true,enableIfAbnormality: "10151010", stopOnNextCast: true},
//     		],
//     		},



// 		"21": {
// 			enabled: true,
// 			key: "Space",
// 			repeater: false,
// 			onCast: [
// 			{ action: "keyRepeat", key: "t", duration: 900, interval: 1, delay: 523, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true},
// 			{ action: "keyRepeat", key: "t", duration: 900, interval: 1, delay: 523, inCombat: true, enableIfAbnormality: "10151010", stopOnNextCast: true},
// 			{ action: "keyRepeat", key: "t", duration: 900, interval: 1, delay: 523, inCombat: true, enableIfAbnormality: "10151192", enableIfSkillCooldown: "16" , stopOnNextCast: true },
// 			],
// 		},
// 		},
// 	hotkeys: {
// 	}
// }














module.exports = {
	enabled: true,
	toggleRepeaterKey: "\\",
	skills: {

		"3": {
			enabled: true,
			key: "3",
			repeater: false,
			onCast: [
            { action: "keyRepeat", key: "Tab", duration: 500, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
			{ action: "keyRepeat", key: "Tab", duration: 500, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151010", stopOnNextCast: true },
            ],
		},

		"4": {
			enabled: false,
			key: "Tab",
			repeater: false,
			onCast: [
            { action: "keyRepeat", key: "space", duration: 1000, interval: 1, delay: 200, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
            ],
		},

        "5": {
			enabled: true,
			key: "t",
			repeater: false,
			onCast: [
				{ action: "keyRepeat", key: "LButton", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
				{ action: "keyRepeat", key: "LButton", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151010", stopOnNextCast: true },
				//{ action: "keyRepeat", key: "Tab", duration: 700, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true }
            ],
		},

		"6": {
			enabled: true,
			key: "f",
			repeater: false,
			onCast: [
			{ action: "keyRepeat", key: "3", duration: 400, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
			{ action: "keyRepeat", key: "3", duration: 400, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151010", stopOnNextCast: true },
            //{ action: "keyRepeat", key: "t", duration: 400, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
            { action: "keyRepeat", key: "3", duration: 400, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151192", stopOnNextCast: true },
            ],
		},


		"12": {
        	enabled: true,
        	key: "4",
        	repeater: false,
        	onCast: [
				//{ action: "keyRepeat", key: "Tab", duration: 500, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
            { action: "keyRepeat", key: "f", duration: 500, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
			{ action: "keyRepeat", key: "f", duration: 500, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151010", stopOnNextCast: true },
            ],
        },

		"19": {
			enabled: false,
			key: "LButton",
			repeater: false,
			onCast: [
            //{ action: "keyRepeat", key: "t", duration: 800, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", enableIfSkillCooldown: "4", stopOnNextCast: true },
		  	{ action: "keyRepeat", key: "t", duration: 800, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016",enableIfSkillCooldown: ["12","4"], stopOnNextCast: true },
		  	{ action: "keyRepeat", key: "4", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016",enableIfSkillCooldown: "4", stopOnNextCast: true },
			  //{ action: "keyRepeat", key: "4", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151010",enableIfSkillCooldown: "12", stopOnNextCast: true },
			//{ action: "keyRepeat", key: "Tab", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151192",enableIfSkillCooldown: "12", stopOnNextCast: true },
            { action: "keyRepeat", key: "Tab", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true },
			//{ action: "keyRepeat", key: "Tab", duration: 1000, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151010", stopOnNextCast: true },
			//{ action: "keyRepeat", key: "4", duration: 800, interval: 1, delay: 1, inCombat: true, enableIfAbnormality: "10151192", stopOnNextCast: true }
            ],
		},

        "16": {
    		enabled: true,
    	    key: "g",
    		repeater: false,
    		onCast: [
    		//{ action: "keyRepeat", key: "LButton", duration: 1000, interval: 1, delay: 0, inCombat: true, stopOnNextCast: true},
			{ action: "keyRepeat", key: "Tab", duration: 400, interval: 1, delay: 0, inCombat: true,enableIfAbnormality: "10151016", stopOnNextCast: true},
			{ action: "keyRepeat", key: "LButton", duration: 400, interval: 1, delay: 0, inCombat: true,enableIfAbnormality: "10151010", stopOnNextCast: true},
    		],
    		},



		"21": {
			enabled: true,
			key: "Space",
			repeater: false,
			onCast: [
			{ action: "keyRepeat", key: "t", duration: 900, interval: 1, delay: 523, inCombat: true, enableIfAbnormality: "10151016", stopOnNextCast: true},
			{ action: "keyRepeat", key: "t", duration: 900, interval: 1, delay: 523, inCombat: true, enableIfAbnormality: "10151010", stopOnNextCast: true},
			{ action: "keyRepeat", key: "t", duration: 900, interval: 1, delay: 523, inCombat: true, enableIfAbnormality: "10151192", enableIfSkillCooldown: "16" , stopOnNextCast: true },
			],
		},
		},
	hotkeys: {
	}
}