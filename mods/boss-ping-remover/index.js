// Set this to your lowest ping -- If you don't have ping-remover
const MY_MIN_PING = 60;

//const bosses = require('./BPR');

class PingClass{
	constructor(){
		this.ping = MY_MIN_PING;
	}
}

class BossPingRemover{
	constructor(dispatch){
		dispatch.hook('C_CHECK_VERSION', 'raw', async ()=> {
			// we use a loader here to avoid race conditions
			await this.installHooks(dispatch);
		});
	}

	async installHooks(dispatch) {
		const {entity, library} = dispatch.require.library;
		const animation_data = await library.query("/AnimationData/");
		const command = dispatch.command;
		try{
			var ping = dispatch.require.ping
		}catch(e){
			var ping = new PingClass();
		}
		let enabled = true;
		let bosses = {};

		command.add('bpr', ()=>{
			enabled = !enabled;
			command.message("Boss ping remover has been " + (enabled?"enabled.":"disabled."));
		});

		async function get_attack_length(skill) {
			const hit_frames = library.getQueryEntry(skill, "TargetingList/Targeting/").map(child=> child.attributes.time / skill.attributes.timeRate);
			let anim_length = 0;
			library.getQueryEntry(skill, "Action/StageList/Stage/AnimSeq/").map(seq=> {
				const anim_data = library.getQueryEntry(animation_data, "AnimSet@name=?/Animation@name=?/", seq.attributes.animSet, seq.attributes.animName)[0];
				if(seq.attributes.readOnlyDuration > 0) {
					anim_length += (seq.attributes.readOnlyDuration - seq.attributes.blendInTime) / seq.attributes.animRate;
				}else {
					anim_length += (anim_data.attributes.animDuration - seq.attributes.blendInTime) / seq.attributes.animRate;
				}
			});

			return Math.max(hit_frames, anim_length / skill.attributes.timeRate);
		}

		async function load_all_boss_info_from_zone(zone) {
			const huntingZoneData = await library.query("/ContinentData/Continent@id=?/HuntingZone/", zone);
			for(const data of huntingZoneData) {
				const huntingZone = data.attributes.id;

				const skills = await library.query("/SkillData@huntingZoneId=?/Skill/", huntingZone);
				for(const skill of skills) {
					const boss_key = `${huntingZone}-${skill.attributes.templateId}-${skill.attributes.id}`;
					bosses[boss_key] = await get_attack_length(skill);
				}
			}
		}

		dispatch.hook('S_LOAD_TOPO', 3, async e=> {
			await load_all_boss_info_from_zone(e.zone);
		});

		dispatch.hook('S_ACTION_STAGE', 9, {order: -5}, e=> {
			if(!enabled) return;
			
			let id = e.gameId.toString();
			let ent = entity.mobs[id];
			if(ent) {
				let length = 0;
				for(let obj of e.animSeq) {
					length += obj.duration;
				}

				if(!length) {
					length = bosses[`${ent.info.huntingZoneId}-${ent.info.templateId}-${e.skill.id}`] || 0;
				}
				let newSpeed = (length / (length - ping.ping)) * e.speed;
				if(newSpeed > e.speed)
					e.speed = newSpeed;
				return true;
			}
		});
	}
	
}

module.exports = BossPingRemover;
