## README 

Small project to review existing leaks of TERA. May include some poorly made scripts.

- Datasheet from original 100.2 leak (with some changes) & Datacenter (Client): https://forum.ragezone.com/f797/tera-level-100-version-1205489/
- Modified tera-toolbox for patch 100.2: https://github.com/tera-private-toolbox
- tera-toolbox mods for patch 100.2: https://github.com/tera-private-mods
- Shinra-toolbox for patch 100.2: https://github.com/Foglio1024/shinra-toolbox-release | https://github.com/tera-private-mods/shinra-toolbox

