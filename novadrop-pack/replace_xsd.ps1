$sourceDir = "D:\novadrop-pack\XSD"
$destDir = "D:\novadrop-pack\DataCenter_Final_EUR"

$sourceXSD = Get-ChildItem -Path $sourceDir -Recurse

$destRepack = Get-ChildItem -Path $destDir -Recurse

foreach($s in $sourceXSD){
    $d = $destRepack | Where-Object{$_.Name -like $s.name}
    Copy-Item $s.FullName -Destination $d.DirectoryName
}